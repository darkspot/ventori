import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/home'
import Catalogue1 from '../screens/catalogues/catalogue1'
import Catalogue2 from '../screens/catalogues/catalogue2'
import Catalogue3 from '../screens/catalogues/catalogue3'
import Catalogue4 from '../screens/catalogues/catalogue4'
import Catalogue5 from '../screens/catalogues/catalogue5'
import Generate1 from '../screens/generateSerials/generate1'
import Generate2 from '../screens/generateSerials/generate2'
import Generate3 from '../screens/generateSerials/generate3'
import Generate4 from '../screens/generateSerials/generate4'
import Scan1 from '../screens/scanSerials/scan1'
import Scan2 from '../screens/scanSerials/scan2'
import Scan3 from '../screens/scanSerials/scan3'
import Inventory from '../screens/inventory'



const Stack = createStackNavigator();

function Route() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#3821C1',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },

      }}
    >
        <Stack.Screen name="HOME" component={Home} />
        <Stack.Screen name="Catalogue1" component={Catalogue1} options={{ title: 'Add Product Catalogue' }} />
        <Stack.Screen name="Catalogue2" component={Catalogue2} options={{ title: 'Add Product Catalogue' }} />
        <Stack.Screen name="Catalogue3" component={Catalogue3} options={{ title: 'Add Product Catalogue' }} />
        <Stack.Screen name="Catalogue4" component={Catalogue4} options={{ title: 'Add Product Catalogue' }} />
        <Stack.Screen name="Catalogue5" component={Catalogue5} options={{ title: 'Add Product Catalogue' }} />
        <Stack.Screen name="Generate1" component={Generate1} options={{ title: 'Generate Serial Numbers' }} />
        <Stack.Screen name="Generate2" component={Generate2} options={{ title: 'Generate Serial Numbers' }} />
        <Stack.Screen name="Generate3" component={Generate3} options={{ title: 'Generate Serial Numbers' }} />
        <Stack.Screen name="Generate4" component={Generate4} options={{ title: 'Generate Serial Numbers' }} />
        <Stack.Screen name="Scan1" component={Scan1} options={{ title: 'Scan Serial Number' }} />
        <Stack.Screen name="Scan2" component={Scan2} options={{ title: 'Scan Serial Number' }} />
        <Stack.Screen name="Scan3" component={Scan3} options={{ title: 'Scan Serial Number' }} />
        <Stack.Screen name="Inventory" component={Inventory} options={{ title: 'Report Inventory' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Route;