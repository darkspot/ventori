import * as React from 'react';
import { DataTable } from 'react-native-paper';

const Inventory = () => (
  <DataTable>
    <DataTable.Header>
      <DataTable.Title>Sizes</DataTable.Title>
      <DataTable.Title>In Stock</DataTable.Title>
      <DataTable.Title>For Delivery</DataTable.Title>
      <DataTable.Title>Released</DataTable.Title>
      <DataTable.Title>Missing</DataTable.Title>
    </DataTable.Header>

    <DataTable.Row>
      <DataTable.Cell>Small (White)</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
    </DataTable.Row>

    <DataTable.Row>
      <DataTable.Cell>Medium (White)</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
    </DataTable.Row>

    <DataTable.Row>
      <DataTable.Cell>Large (White)</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
      <DataTable.Cell numeric>3</DataTable.Cell>
    </DataTable.Row>

    <DataTable.Pagination
      page={1}
      numberOfPages={3}
      onPageChange={page => {
        console.log(page);
      }}
      label="1-2 of 6"
    />
  </DataTable>
);

export default Inventory;