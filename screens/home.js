import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";

function Home(props) {
  return (
    <View style={styles.container}>
      <View style={styles.group2}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Catalogue1")}
          style={styles.button}
        ></TouchableOpacity>
        <Text style={styles.addProductCatalogue}>Add Product Catalogue</Text>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Inventory")}
          style={styles.button2}
        ></TouchableOpacity>
        <Text style={styles.reportInventory}>Report Inventory</Text>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Scan1")}
          style={styles.button3}
        ></TouchableOpacity>
        <Text style={styles.scanSerialNumber}>Scan Serial Number</Text>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Generate1")}
          style={styles.button4}
        ></TouchableOpacity>
        <Text style={styles.generateSerialNumber}>Generate Serial Number</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center"
  },
  group2: {
    height: 453,
    overflow: "scroll",
    justifyContent: "space-between"
  },
  button: {
    height: 56,
    backgroundColor: "#E6E6E6",
    alignSelf: "stretch",
    marginTop: 0,
    marginBottom: 0
  },
  addProductCatalogue: {
    top: 23,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    left: 47
  },
  button2: {
    height: 56,
    backgroundColor: "#E6E6E6",
    alignSelf: "stretch",
    marginTop: 0,
    marginBottom: 0
  },
  reportInventory: {
    top: 147,
    left: 47,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212"
  },
  button3: {
    height: 56,
    backgroundColor: "#E6E6E6",
    alignSelf: "stretch",
    marginTop: 0,
    marginBottom: 0
  },
  scanSerialNumber: {
    top: 285,
    left: 47,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212"
  },
  button4: {
    height: 56,
    backgroundColor: "#E6E6E6",
    alignSelf: "stretch",
    marginTop: 0,
    marginBottom: 0
  },
  generateSerialNumber: {
    top: 415,
    left: 47,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212"
  }
});

export default Home;
