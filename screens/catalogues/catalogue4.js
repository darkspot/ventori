import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";

function Catalogue4(props) {
  return (
    <View style={styles.container}>
      <View style={styles.blueColumn}>
        <Text style={styles.blue}>blue</Text>
        <Text style={styles.yellow}>yellow</Text>
        <Text style={styles.red}>red</Text>
      </View>
      <View style={styles.blueColumnFiller}></View>
      <View style={styles.groupColumn}>
        <View style={styles.group}>
          <View style={styles.buttonRow}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Catalogue5")}
              style={styles.button}
            >
              <Text style={styles.done}>Done</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Catalogue5")}
              style={styles.button2}
            >
              <Text style={styles.addInventory}>Add Inventory</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.rect}>
          <View style={styles.rect2}></View>
          <View style={styles.rect3}></View>
          <View style={styles.rect4}></View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 0,
    borderColor: "rgba(130,56,255,1)"
  },
  blue: {
    fontFamily: "roboto-regular",
    color: "#121212"
  },
  yellow: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 51,
    marginLeft: 2
  },
  red: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 49,
    marginLeft: 2
  },
  blueColumn: {
    width: 41,
    marginTop: 225,
    marginLeft: 46
  },
  blueColumnFiller: {
    flex: 1
  },
  group: {
    height: 35,
    flexDirection: "row",
    marginBottom: 126
  },
  button: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100
  },
  done: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 20
  },
  button2: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100,
    marginLeft: 79
  },
  addInventory: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 7
  },
  buttonRow: {
    height: 35,
    flexDirection: "row",
    flex: 1,
    marginRight: 39,
    marginLeft: 40
  },
  rect: {
    width: 132,
    height: 16,
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    alignSelf: "center"
  },
  rect2: {
    flex: 0.33,
    backgroundColor: "rgba(253, 253, 253,1)"
  },
  rect3: {
    flex: 0.33,
    backgroundColor: "rgba(218, 218, 218,1)"
  },
  rect4: {
    flex: 0.34,
    backgroundColor: "rgba(235, 235, 235,1)"
  },
  groupColumn: {
    marginBottom: 58
  }
});

export default Catalogue4;
