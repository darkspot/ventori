import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  TouchableOpacity
} from "react-native";

function Catalogue1(props) {
  return (
    <View style={styles.container}>
      <View style={styles.placeholder1Column}>
        <TextInput placeholder="" style={styles.placeholder1}></TextInput>
        <Text style={styles.loremIpsum}>Lorem Ipsum</Text>
      </View>
      <View style={styles.placeholder1ColumnFiller}></View>
      <View style={styles.groupColumn}>
        <View style={styles.group}>
          <View style={styles.buttonRow}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.button}
            >
              <Text style={styles.back}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Catalogue2")}
              style={styles.button2}
            >
              <Text style={styles.next}>Next</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.rect}>
          <View style={styles.rect2}></View>
          <View style={styles.rect3}></View>
          <View style={styles.rect4}></View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 0,
    borderColor: "rgba(130,56,255,1)"
  },
  placeholder1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    height: 38,
    width: 270,
    borderWidth: 0,
    borderColor: "#000000",
    borderBottomWidth: 1,
    marginTop: 59
  },
  loremIpsum: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: -97,
    marginLeft: 21
  },
  placeholder1Column: {
    width: 270,
    marginTop: 205,
    marginLeft: 45
  },
  placeholder1ColumnFiller: {
    flex: 1
  },
  group: {
    height: 35,
    flexDirection: "row",
    marginBottom: 126
  },
  button: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100
  },
  back: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 20
  },
  button2: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100,
    marginLeft: 79
  },
  next: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 21
  },
  buttonRow: {
    height: 35,
    flexDirection: "row",
    flex: 1,
    marginRight: 39,
    marginLeft: 40
  },
  rect: {
    width: 132,
    height: 16,
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    alignSelf: "center"
  },
  rect2: {
    flex: 0.33,
    backgroundColor: "rgba(253, 253, 253,1)"
  },
  rect3: {
    flex: 0.33,
    backgroundColor: "rgba(218, 218, 218,1)"
  },
  rect4: {
    flex: 0.34,
    backgroundColor: "rgba(235, 235, 235,1)"
  },
  groupColumn: {
    marginBottom: 58
  }
});

export default Catalogue1;
