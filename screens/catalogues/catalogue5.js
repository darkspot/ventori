import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

function Catalogue5(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.popup}>popup</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  popup: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 220,
    marginLeft: 114
  }
});

export default Catalogue5;
