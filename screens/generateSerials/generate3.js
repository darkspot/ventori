import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

function Generate3(props) {
  return (
    <View style={styles.container}>
      <View style={styles.loremIpsum1Column}>
        <Text style={styles.loremIpsum1}>Lorem Ipsum</Text>
        <Icon name="qrcode" style={styles.icon1}></Icon>
      </View>
      <View style={styles.loremIpsum1ColumnFiller}></View>
      <View style={styles.group1Column}>
        <View style={styles.group1}>
          <View style={styles.button1Row}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.button1}
            >
              <Text style={styles.back1}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Generate4")}
              style={styles.button2}
            >
              <Text style={styles.next1}>Next</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.rect1}>
          <View style={styles.rect2}></View>
          <View style={styles.rect3}></View>
          <View style={styles.rect4}></View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loremIpsum1: {
    fontFamily: "roboto-regular",
    color: "#121212"
  },
  icon1: {
    color: "rgba(128,128,128,1)",
    fontSize: 102,
    marginTop: 72,
    marginLeft: 41
  },
  loremIpsum1Column: {
    width: 143,
    marginTop: 205,
    marginLeft: 66
  },
  loremIpsum1ColumnFiller: {
    flex: 1
  },
  group1: {
    height: 35,
    flexDirection: "row",
    marginBottom: 126
  },
  button1: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100
  },
  back1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 20
  },
  button2: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100,
    marginLeft: 79
  },
  next1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 21
  },
  button1Row: {
    height: 35,
    flexDirection: "row",
    flex: 1,
    marginRight: 39,
    marginLeft: 40
  },
  rect1: {
    width: 132,
    height: 16,
    backgroundColor: "#E6E6E6",
    flexDirection: "row",
    alignSelf: "center"
  },
  rect2: {
    flex: 0.33,
    backgroundColor: "rgba(253, 253, 253,1)"
  },
  rect3: {
    flex: 0.33,
    backgroundColor: "rgba(218, 218, 218,1)"
  },
  rect4: {
    flex: 0.34,
    backgroundColor: "rgba(235, 235, 235,1)"
  },
  group1Column: {
    marginBottom: 58
  }
});

export default Generate3;
