import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

function Generate4(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.button1}
      ></TouchableOpacity>
      <Text style={styles.loremIpsum1}>Lorem Ipsum</Text>
      <Icon name="qrcode" style={styles.icon}></Icon>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  button1: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100,
    marginTop: 501,
    marginLeft: 107
  },
  loremIpsum1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: -331,
    marginLeft: 66
  },
  icon: {
    color: "rgba(128,128,128,1)",
    fontSize: 102,
    marginTop: 72,
    marginLeft: 107
  }
});

export default Generate4;
