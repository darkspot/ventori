import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import EntypoIcon from "react-native-vector-icons/Entypo";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

function Scan2(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.loremIpsum3}>Lorem Ipsum</Text>
      <TouchableOpacity
        onPress={() => props.navigation.navigate("Scan3")}
        style={styles.button}
      >
        <EntypoIcon name="camera" style={styles.icon}></EntypoIcon>
      </TouchableOpacity>
      <MaterialCommunityIconsIcon
        name="qrcode"
        style={styles.icon1}
      ></MaterialCommunityIconsIcon>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loremIpsum3: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 123,
    marginLeft: 80
  },
  button: {
    width: 99,
    height: 68,
    backgroundColor: "#E6E6E6",
    borderRadius: 100,
    marginTop: 393,
    marginLeft: 121
  },
  icon: {
    color: "rgba(128,128,128,1)",
    fontSize: 62,
    height: 68,
    width: 62,
    marginLeft: 19
  },
  icon1: {
    color: "rgba(128,128,128,1)",
    fontSize: 189,
    marginTop: -400,
    marginLeft: 80
  }
});

export default Scan2;
