import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

function Scan3(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.summary}>Summary</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  summary: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 142,
    marginLeft: 89
  }
});

export default Scan3;
