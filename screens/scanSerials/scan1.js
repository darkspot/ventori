import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

function Scan1(props) {
  return (
    <View style={styles.container}>
      <View style={styles.placeholderColumn}>
        <TextInput placeholder="" style={styles.placeholder}></TextInput>
        <Text style={styles.loremIpsum}>Lorem Ipsum</Text>
        <Text style={styles.loremIpsum3}>Lorem Ipsum</Text>
        <Icon name="qrcode" style={styles.icon}></Icon>
      </View>
      <View style={styles.placeholderColumnFiller}></View>
      <View style={styles.group1}>
        <View style={styles.button1Row}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Scan2")}
            style={styles.button1}
          >
            <Text style={styles.takePhoto}>Take Photo</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={styles.button2}
          >
            <Text style={styles.done}>Done</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  placeholder: {
    fontFamily: "roboto-regular",
    color: "#121212",
    height: 38,
    width: 270,
    borderWidth: 0,
    borderColor: "#000000",
    borderBottomWidth: 1,
    marginTop: 346
  },
  loremIpsum: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: -78,
    marginLeft: 17
  },
  loremIpsum3: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: -323,
    marginLeft: 25
  },
  icon: {
    color: "rgba(128,128,128,1)",
    fontSize: 189,
    marginTop: 41,
    marginLeft: 17
  },
  placeholderColumn: {
    width: 270,
    marginTop: 123,
    marginLeft: 55
  },
  placeholderColumnFiller: {
    flex: 1
  },
  group1: {
    height: 35,
    width: 360,
    flexDirection: "row",
    marginBottom: 124,
    alignSelf: "center"
  },
  button1: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100
  },
  takePhoto: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 20
  },
  button2: {
    width: 101,
    height: 35,
    borderWidth: 2,
    borderColor: "rgba(130,56,255,1)",
    borderStyle: "solid",
    borderRadius: 100,
    marginLeft: 79
  },
  done: {
    fontFamily: "roboto-regular",
    color: "#121212",
    marginTop: 9,
    marginLeft: 21
  },
  button1Row: {
    height: 35,
    flexDirection: "row",
    flex: 1,
    marginRight: 39,
    marginLeft: 40
  }
});

export default Scan1;
